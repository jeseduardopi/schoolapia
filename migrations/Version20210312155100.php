<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210312155100 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE matiere_emploi_du_temps (matiere_id INT NOT NULL, emploi_du_temps_id INT NOT NULL, INDEX IDX_21964269F46CD258 (matiere_id), INDEX IDX_21964269C13CD51E (emploi_du_temps_id), PRIMARY KEY(matiere_id, emploi_du_temps_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE matiere_emploi_du_temps ADD CONSTRAINT FK_21964269F46CD258 FOREIGN KEY (matiere_id) REFERENCES matiere (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE matiere_emploi_du_temps ADD CONSTRAINT FK_21964269C13CD51E FOREIGN KEY (emploi_du_temps_id) REFERENCES emploi_du_temps (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE matiere_emploi_du_temps');
    }
}
